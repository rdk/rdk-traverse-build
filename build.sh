#!/bin/bash
set -e
OUTPUT_DIR=$(readlink -f output)
mkdir -p "${OUTPUT_DIR}"

DEPLOY_PATH="tmp/deploy/images/armefi64-rdk-broadband"
WIC_FILE_NAME="rdk-generic-broadband-image-armefi64-rdk-broadband.wic"
WIC_FILE_PATH="${DEPLOY_PATH}/${WIC_FILE_NAME}"
TGZ_FILE_NAME="rdk-generic-broadband-image-armefi64-rdk-broadband.tar.gz"
TGZ_FILE_PATH="${DEPLOY_PATH}/${TGZ_FILE_NAME}"
DEBUG_FILE_NAME="rdk-generic-broadband-image-armefi64-rdk-broadband-dbg.tar.xz"
DEBUG_FILE_PATH="${DEPLOY_PATH}/${DEBUG_FILE_NAME}"

if [ -n "${CI}" ]; then
	echo "CI Build, Setting git user.name and user.email"
	git config --global user.email "cibuild@example.com"
	git config --global user.name "CI Build User"
fi

# < /dev/null redirect to avoid repo asking questions to the terminal (like enabling colored text)
repo init -u 'https://gitlab.traverse.com.au/rdk/manifests.git' -m rdkb-extsrc-traverse.xml -b "traverse/cmfarm" < /dev/null
# meta-rdk sync tends to stall, so do it first
repo sync meta-rdk < /dev/null
repo sync < /dev/null
echo "------------------------------------------------"
echo "List of repos:"
ls -la
echo "------------------------------------------------"

if [ ! -f "meta-cmf-arm/setup-environment" ]; then
	echo "ERROR: repos did not clone correctly"
	exit 1
fi
. ./meta-cmf-arm/setup-environment
# Generate debug rootfs for later debugging (gdb etc.)
echo 'IMAGE_GEN_DEBUGFS = "1"' >> "conf/local.conf"
echo 'IMAGE_FSTYPES_DEBUGFS = "tar.xz"' >> "conf/local.conf"

bitbake rdk-generic-broadband-image

pwd
echo "Build complete, uploading images"
echo "wic uncompressed size: "
du -h $(readlink -f "${WIC_FILE_PATH}")
cp "${WIC_FILE_PATH}" .
echo "Converting to compressed qcow2"
qemu-img convert -O qcow2 -c "${WIC_FILE_NAME}" "${WIC_FILE_NAME}.qcow2"
du -h "${WIC_FILE_NAME}.qcow2"
cp "${WIC_FILE_NAME}.qcow2" "${OUTPUT_DIR}"

cp "${DEBUG_FILE_PATH}" "${OUTPUT_DIR}"
cp "${TGZ_FILE_PATH}" "${OUTPUT_DIR}"
echo "Output dir including debug rootfs:"
ls -la "${OUTPUT_DIR}"
