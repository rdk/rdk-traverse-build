# RDK builds for Ten64 and Arm generic / EFI


## Supported build hosts
This builder has been used on both Arm64 and x86-64 hosts. CI builds by Traverse
use Arm64 hosts from Hetzner.

## Container based build

Using podman (or docker):

```
podman build -t rdk-build build-container
podman run --rm -i -t -v $(pwd):/tmp/build-scripts rdk-build
podman exec -i -t rdk-build /bin/bash -l
# In the container
cp -r /tmp/build-scripts .
cd build-scripts
./build.sh
```

The final image will be placed in the `output` directory, as an xz-compressed image

# Running the generated image

## QEMU (Arm64 virtualized or emulated)
You will need an EFI  or U-Boot 'BIOS' binary, such as from `qemu-efi-aarch64`
in Debian (EDK2), [retrage/edk2-nightly](https://retrage.github.io/edk2-nightly/).
or U-Boot's qemu aarch64 target (you can find a prebuilt binary [here](http://downloads.openwrt.org/releases/23.05.4/targets/armsr/armv8/u-boot-qemu_armv8/) ).

On virtualization capable Arm64 hosts (Ten64, AWS a1.metal) you can use KVM for near-native performance.

You can invoke QEMU like this:

```
# Convert the WIC image to qcow2. This is optional but allows you
# to use features like snapshotting.
unxz rdk.img.xz
qemu-img convert -O qcow2 rdk.img rdk.qcow2

# It can be helpful to create a snapshot to quickly revert
# any changes that are made
qemu-img create -f qcow2 -b rdk.qcow2 -F qcow2 rdk.resize.qcow2
# And resize that snapshot to allow space for upgrades
qemu-img resize rdk.resize.qcow2 10G

DISK=${DISK:-rdk.resize.qcow2}
qemu-system-aarch64 -nographic \
    -cpu host --enable-kvm -machine virt \
    -bios u-boot.bin \
    -smp 2 -m 1024 \
    -device virtio-rng-pci \
    -hda "${DISK}" \
    -netdev tap,id=testlan -net nic,netdev=testlan \
    -netdev tap,id=testlan2 -net nic,netdev=testlan2 \
    -netdev user,id=testwan -net nic,netdev=testwan \
    -device pci-serial
```

On other host platforms, replace `-cpu host --enable-kvm` with `-cpu cortex-a53`.

## Ten64

Use the [recovery environment](https://ten64doc.traverse.com.au/quickstart/#recovery-environment) to
stage and write the image to disk.

To speed up the process, we recommend converting the .img.xz from the build to qcow2, then transferring
to the Ten64-Recovery over SSH/SCP.

```
# On workstation
unxz rdk.img.xz
qemu-img convert -O qcow2 -C rdk.img rdk.qcow2
scp rdk.qcow2 root@192.168.10.1:/tmp

# On Ten64
blkdiscard -f /dev/nvme0n1
write-image-disk rdk.qcow2 /dev/nvme0n1
reboot
```

